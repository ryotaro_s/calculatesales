package jp.alhinc.sugita_ryotaro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class CalculateSales{
	public static void main(String[] args) {
		String path = args[0];		//売上ファイル読み込み
	    File dir = new File(path);
	    File[] files = dir.listFiles();
	    Map<String, String> map = new LinkedHashMap<String, String>();
		Map<String, Object> map2 = new LinkedHashMap<String, Object>();

		BufferedReader br = null,br2 = null;

		try{
			File file = new File(args[0], "branch.lst");//支店定義ファイルの読み込み
			if(file.exists()){	//定義ファイルがあるかチェック
			}else{
				System.out.println("支店定義ファイルが存在しません");
				return;		//支店定義ファイルがないため処理終了
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			while((line=br.readLine())!=null){
				String pattern = "[0-9]{3}";
				Pattern p = Pattern.compile(pattern);
				String[] seles = line.split(",");		//支店番号と支店名に分割
				if((p.matcher(line).find())&&(seles.length == 2)){		//3桁かつ要素数2
				}else{
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				map.put(seles[0],seles[1]);		//キーと値をマッピング
				}


				for (int i = 0; i < files.length; i++) {
					String str = files[i].getName();
					String pattern2 = "[0-9]{8}.*rcd$";		//8桁かつrcdファイル
					Pattern p2 = Pattern.compile(pattern2);
			    	if(p2.matcher(str).find()){		//正規表現で検索
						File file2 = new File(args[0],str);
						FileReader fr2 = new FileReader(file2);
						br2 = new BufferedReader(fr2);

						String[] sn = files[i].getName().split("\\.");	//ファイル名と拡張子に分割
						int snint = Integer.parseInt(sn[0]);			//ファイル名をint型に変換
							if(i>1){									//2周目以降は前の番号より1大きいかチェック
								String[] sn2 = files[i-1].getName().split("\\.");
								int snint2 = Integer.parseInt(sn2[0]);
									if(snint-snint2==1){
									}else{
										System.out.println("売上ファイル名が連番になっていません");
										fr2.close();
										return;		//処理終了
									}
							}


						String lines,lines2;		//支店番号,売上
						lines= br2.readLine();
						lines2= br2.readLine();
							LineNumberReader in = new LineNumberReader(br2);	//3行目以降が存在したら終了
							while((in.readLine())!=null){
									if(in.getLineNumber() > 0){
									System.out.println(files[i].getName()+"のフォーマットが不正です");
									in.close();
									return;		//処理終了
								}
							}
							in.close();

							if(map2.containsKey(lines)){	//キーが既に入っていたら加算
								long sum = Long.parseLong(lines2);
								long sum2 = Long.parseLong((String) map2.get(lines));
								sum2 += sum;
									if(String.valueOf(sum2).length()>10){		//合計金額10桁超え確認
										System.out.println("合計金額が10桁を超えました");
										return;		//処理終了
									}
								map2.put(lines,sum2);
							}else{
								map2.put(lines,lines2);	//キーと値をマッピング
							}
						}
					}


			    for (String key : map2.keySet()) { // map2のkeyの数だけループ
			        if (map.containsKey(key)) {
			        	File fileout = new File(args[0], "branch.out");
			        	FileWriter fw = new FileWriter(fileout,true);		//ファイルに追記
			        	BufferedWriter bw = new BufferedWriter(fw);
			        	bw.write(key + "," + map.get(key)+","+map2.get(key));
			        	bw.newLine();
			        	bw.close();
			        }else{
					    System.out.println("支店コードが不正です");
					    return;
					}
			    }

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
		}finally{
			if(br != null){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("closeできませんでした");
				}
			}
		}
	}
}
